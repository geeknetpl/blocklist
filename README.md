# Blocklists
Blocklists contains a list of lists that contain domains that meet certain parameters. 
They are used for scanning, tracking or displaying ads etc.
You can select all or one of them to block specific activity in your network.

## Shodan blocklist
The list contains domains used by the scanner and the collector of devices connected to the Internet.
This list is best used to build firewall rules.

# How was data collected for blacklists?
Domain names were collected from traffic analysis and DNS queries on my networks. 
The domains were then selected for blacklists based on keywords and behavior.
